package uz.beproedu.springedu.controller;

import org.springframework.web.bind.annotation.*;
import uz.beproedu.springedu.model.User;
import uz.beproedu.springedu.service.UserService;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> findAll(){
        return userService.findAll();
    }

    @GetMapping("{id}")
    public User findById(@PathVariable Long id){
        return userService.findById(id);
    }

    @PostMapping
    public User add(@RequestBody User user){
        return userService.add(user);
    }

    @PutMapping("{id}")
    public User edit(@PathVariable Long id, @RequestBody User user){
        return userService.edit(id, user);
    }

    @DeleteMapping("{id}")
    public User delete(@PathVariable Long id){
        return userService.delete(id);
    }
}