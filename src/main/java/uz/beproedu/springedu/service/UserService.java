package uz.beproedu.springedu.service;

import uz.beproedu.springedu.model.User;

import java.util.List;

public interface UserService {
    User findById(Long id);
    List<User> findAll();
    User add(User user);
    User edit(Long id, User user);
    User delete(Long id);
}
