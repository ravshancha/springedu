package uz.beproedu.springedu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.beproedu.springedu.model.User;
import uz.beproedu.springedu.repository.UserRepository;
import uz.beproedu.springedu.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repo;

    @Override
    public User findById(Long id) {
        Optional<User> userOptional = repo.findById(id);
        return userOptional.orElseGet(User::new);
    }

    @Override
    public List<User> findAll() {
        return repo.findAll();
    }

    @Override
    public User add(User user) {
        return repo.save(user);
    }

    @Override
    public User edit(Long id, User user) {
        Optional<User> userOptional = repo.findById(id);
        if (userOptional.isPresent()) {
            userOptional.get().setUsername(user.getUsername());
            return repo.save(userOptional.get());
        } else {
            return new User();
        }
    }

    @Override
    public User delete(Long id) {
        Optional<User> userOptional = repo.findById(id);
        if (userOptional.isPresent()) {
            repo.deleteById(id);
            return userOptional.get();
        } else {
            return new User();
        }
    }
}