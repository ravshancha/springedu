package uz.beproedu.springedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEduApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringEduApplication.class, args);
    }
}