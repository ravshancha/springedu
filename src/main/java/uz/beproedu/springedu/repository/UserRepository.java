package uz.beproedu.springedu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.beproedu.springedu.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
